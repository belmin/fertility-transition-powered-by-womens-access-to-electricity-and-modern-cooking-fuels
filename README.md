# Fertility transition powered by womens access to electricity and modern cooking fuels

Companion repository for publication "Fertility transition powered by women's access to electricity and modern cooking fuels" *Nature Sustainability* (2021)
by Belmin Camille, Hoffmann Roman, Pichler Peter-Paul and Weisz Helga.  https://www.nature.com/articles/s41893-021-00830-3 

This repository contains all code and documentation to reproduce the:

1) [Code to pre-process the DHS data](1_pre_processing/pre_processing.Rmd) to reproduce the panel data set used in the analysis
2) [Reproducible manuscript](2_manuscript/manuscript.Rmd)  with text, results and figures
3) [Reproducible supplementary information file](3_supplementary_information/supplementary_information.Rmd) with text, results and figures

The source data of each figure of the manuscript can be produced in the file [2_manuscript/manuscript.Rmd](2_manuscript/manuscript.Rmd), in the commented code below the figures. 

